<?php


namespace App\Http\Controllers;


use Laravel\Socialite\Facades\Socialite;

class TestAuthController extends Controller
{
    public function index()
    {
        return view('TestAuth.index');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    public function redirectToProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        dd($user);
    }
}