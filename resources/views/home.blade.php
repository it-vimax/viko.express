





<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Kabanchik.ua - онлайн-сервис заказа услуг</title>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="1535482163##4161ad1051ef0fd02fee819077b97f4599f9576b">
    <meta name="country-code" content="UA">
    <meta name="assets-base" content="https://static.kabanchik.ua/static/">
    <meta name="description" content='Сервис поиска исполнителей для бытовых и бизнес задач: мастера, фрилансеры, уборщики, курьеры, перевозчики - удобный способ заказать любую услугу'>
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta property="og:title" content="Kabanchik.ua - онлайн-сервис заказа услуг" />
    <meta property="og:description" content='Сервис поиска исполнителей для бытовых и бизнес задач: мастера, фрилансеры, уборщики, курьеры, перевозчики - удобный способ заказать любую услугу' />
    <meta property="og:image" content="/images/favicons/logo-medium-rect.png" />
    <meta property="og:image:width" content="736">
    <meta property="og:image:height" content="383">
    <meta property="og:url" content="https://kabanchik.ua/">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Kabanchik.ua"/>




    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicons/apple-icon-72x72.png?r=b5e8129c">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-icon-76x76.png?r=94d9fabd">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicons/apple-icon-114x114.png?r=bf547124">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicons/apple-icon-120x120.png?r=e5f77aab">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicons/apple-icon-144x144.png?r=5f01eea0">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicons/apple-icon-152x152.png?r=df3372bf">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-icon-180x180.png?r=9d20434a">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicons/android-icon-192x192.png?r=bea9e68f">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png?r=06388220">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicons/favicon-96x96.png?r=2458b8f1">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png?r=2638f927">
    <link rel="manifest" href="/images/favicons/manifest.json?r=d11febf9">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicons/ms-icon-144x144.png?r=5f01eea0">
    <meta name="theme-color" content="#ffffff">

    <link href="/css/responsive/main.css?r=be90357d" media="all" rel="stylesheet" type="text/css" />



    <script>dataLayer=[]</script>
    <!-- Page hiding snippet (recommended) -->
    <style>.async-hide { opacity: 0 !important;}</style>
    <script>
        (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);
        })(window,document.documentElement,'async-hide','dataLayer',3000,{'GTM-NQLQN97':true});
    </script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-60334366-1', 'auto', {'useAmpClientId': true});
        ga('require', 'GTM-NQLQN97');
    </script>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MS9GSG');</script>

    <meta name="google-site-verification" content="tiCIkqOEzQh-y7pMG3oAhhR3utxU7DmV3k_a8o0D6kw" />
    <meta name="yandex-verification" content="6b30d7e8288106f8" />


    <meta name="verify-reformal" content="2f5ee755b7427661b5cdf239" />

    <meta property="fb:admins" content="1781674310" />
    <meta property="fb:app_id" content="331515400276587" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@kabanchikom" />
    <meta name="twitter:title" content="Kabanchik.ua - онлайн-сервис заказа услуг" />
    <meta name="twitter:description" content="Сервис поиска исполнителей для бытовых и бизнес задач: мастера, фрилансеры, уборщики, курьеры, перевозчики - удобный способ заказать любую услугу" />
    <meta name="twitter:image" content="/images/logo/logo-medium-rect.png?r=6d1796c7" />


</head>
<body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS9GSG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<div class="kb-layout">
    <div class="kb-layout__row">

























        <div class="kb-layout__header">
            <div class="kb-header qa-header" data-bazooka="Header" data-type="header">
                <div class="kb-page">
                    <div class="kb-header__wrapper">
                        <div class="kb-header__logo kb-header__logo_user_not-authorized">
                            <a class="kb-header__logo-link" href="https://kabanchik.ua/">
                                <img class="kb-header__logo-img"

                                     src="/images/logo/logo.png?r=af703f29"
                                     srcset="/images/logo/logo2x.png?r=10036b79"

                                     alt="Kabanchik.ua"
                                     width="143"
                                     height="34"
                                >
                            </a>
                        </div>






                        <div class="kb-header__dropdown-button qa-header-categories-ab"
                             data-bazooka="HeaderCategories"
                             data-title="Создать задание"
                             data-type-customer="true"
                             data-categories-url="/category/all?include_url=0"
                        >

                            <div class="kb-categories-dropdown kb-categories-dropdown_type_header">
                                <div class="kb-categories-dropdown__button kb-categories-dropdown__button_glyph_arrow-down">
                                <span class="kb-categories-dropdown__button-text">
                                    Создать задание
                                </span>
                                </div>
                            </div>
                        </div>










                        <div class="kb-header__authorization
            
        ">
                            <ul class="kb-header__authorization-list">
                                <li class="kb-header__signin">
                                    <a href="https://kabanchik.ua/auth/login" class="kb-header__signin-link">
                                        Войти
                                    </a>
                                </li>
                                <li class="kb-header__signup">
                                    <a
                                            href="https://kabanchik.ua/registration/performer"
                                            class="b-button b-button_width_full b-button_type_header-signup b-button_theme_bordered"
                                    >
                        <span class="b-button__text">
                            Стать исполнителем
                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>





                        <div class="kb-header__search-holder">
                            <div class="kb-header__search-aligner">
                                <form action="/task/create" class="kb-plain-search kb-plain-search_size_small">
                                    <div class="kb-plain-search__wrapper">
                                        <button
                                                class="kb-plain-search__button b-button"
                                                type="submit"
                                        >
                                        <span class="kb-plain-search__text b-button__text">
                                            Найти специалиста
                                        </span>
                                        </button>
                                        <div class="kb-plain-search__field">
                                            <input name="q" class="kb-plain-search__input"
                                                   placeholder="Что нужно сделать?" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <span class="kb-header__fader"></span>
                    </div>
                </div>
            </div>
        </div>
        <!--[if lte IE 9]>
        <div class="kb-page">
            <div class="b-browser-update">
                <div class="b-browser-update__icon icon-warning"></div>
                <div class="b-browser-update__content">
                    <div class="b-browser-update__title">ВНИМАНИЕ!</div>
                    <p>Вы используете устаревшую версию браузера Internet Explorer</p>
                    <p>При использовании Internet Explorer 10 и младше, возможна некорректная и медленная работа сайта, часть функционала может быть недоступна.<br>Настоятельно рекомендуем вам выбрать и установить любой из современных браузеров. Это бесплатно и займет всего несколько минут.</p>
                    <ul class="b-browser-update__columns">
                        <li class="b-browser-update__column">
                            <a href="https://www.google.com/chrome/" target="_blank">
                                <img class="b-browser-update__img" alt="Google Chrome" src="/images/browser_chrome.png?r=f21d3967" /><span class="b-browser-update__browser-name">Google Chrome</span>
                            </a>
                        </li>
                        <li class="b-browser-update__column">
                            <a href="http://www.mozilla.com/firefox/" target="_blank">
                                <img class="b-browser-update__img" alt="Mozilla Firefox" src="/images/browser_firefox.png?r=513fe307" /><span class="b-browser-update__browser-name">Mozilla Firefox</span>
                            </a>
                        </li>
                        <li class="b-browser-update__column">
                            <a href="http://www.microsoft.com/windows/Internet-explorer/default.aspx" target="_blank">
                                <img class="b-browser-update__img" alt="Internet Explorer" src="/images/browser_ie.png?r=35864d4b" /><span class="b-browser-update__browser-name">Internet Explorer</span>
                            </a>
                        </li>
                        <li class="b-browser-update__column">
                            <a href="http://www.opera.com/download/" target="_blank">
                                <img class="b-browser-update__img" alt="Opera" src="/images/browser_opera.png?r=66988159" /><span class="b-browser-update__browser-name">Opera</span>
                            </a>
                        </li>
                    </ul>
                    <p>Если по каким-либо причинам вы не имеете доступа к возможности установки программ, то рекомендуем воспользоваться "portable" версиями браузеров. Они не требуют инсталляции на компьютер и работают с любого диска или вашей флешки: <a href="http://portableapps.com/apps/internet/firefox_portable" target="_blank">Mozilla Firefox</a> или <a href="http://portableapps.com/apps/internet/google_chrome_portable" target="_blank">Google Chrome</a>
                    </p>
                </div>
            </div>
        </div>
        <![endif]-->




    </div>

    <div class="kb-layout__row kb-layout__row_type_content">
        <div class="kb-page">

            <div class="kb-promotion kb-promotion_type_main ">
                <div class="kb-promotion__content">
                    <h1 class="kb-promotion__title">
                        Онлайн-сервис заказа услуг
                    </h1>
                    <p class="kb-promotion__subtitle">
                        Более 100 000 проверенных специалистов для выполнения ваших бытовых или бизнес задач
                    </p>
                    <div class="kb-promotion__plain-search">
                        <form
                                action="/task/create"
                                class="kb-plain-search kb-plain-search_size_medium kb-plain-search_max-width_600"
                        >
                            <div class="kb-plain-search__wrapper">
                                <button class="kb-plain-search__button b-button" type="submit">
                            <span class="kb-plain-search__text b-button__text">
                                Найти специалиста
                            </span>
                                </button>
                                <div class="kb-plain-search__field">
                                    <input id="searchInput" name="q" class="kb-plain-search__input"
                                           placeholder="Что нужно сделать?" autofocus />
                                </div>
                            </div>
                        </form>
                        <div class="kb-promotion__search-hint">
                            Например:
                            <span
                                    class="kb-promotion__search-hint-link"
                                    onclick="document.getElementById('searchInput').value = 'убрать в квартире';document.getElementById('searchInput').focus()"
                            >убрать в квартире</span>
                        </div>
                    </div>
                </div>
                <div class="kb-promotion__anchors">
                    <div class="kb-anchors">
                        <div class="kb-anchors__item">
                            <a class="kb-anchors__link"
                               data-bazooka='ScrollTo'
                               data-selector='#all-categories'
                               data-duration='1000'
                               data-easing='expoOut'
                               data-delta='60'
                            >
                                <i class="kb-anchors__icon icon-squares"></i>
                                <span class="kb-anchors__text">Все категории услуг</span>
                            </a>
                        </div>
                        <div class="kb-anchors__item">
                            <a class="kb-anchors__link"
                               data-bazooka='ScrollTo'
                               data-selector='#hiw'
                               data-duration='1000'
                               data-easing='expoOut'
                               data-delta='100'
                            >
                                <i class="kb-anchors__icon icon-user"></i>
                                <span class="kb-anchors__text">Как это работает</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kb-divisions">
                <ul class="kb-divisions__list">
                    <li class="kb-divisions__item ">

                        <a
                                href="https://kiev.kabanchik.ua/home"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-house"></i>Дом</a>


                    </li><li class="kb-divisions__item ">

                        <a
                                href="https://kiev.kabanchik.ua/delivery"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-delivery"></i>Доставка</a>


                    </li><li class="kb-divisions__item ">

                        <a
                                href="https://kiev.kabanchik.ua/freelance"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-mouse"></i>Фриланс</a>


                    </li><li class="kb-divisions__item ">

                        <a
                                href="https://kiev.kabanchik.ua/teachers"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-book"></i>Преподаватели</a>


                    </li><li class="kb-divisions__item ">

                        <a
                                href="https://kiev.kabanchik.ua/business"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-bag"></i>Бизнес</a>


                    </li>
                    <li class="kb-divisions__item">
                        <a
                                href="https://kiev.kabanchik.ua/all-categories"
                                class="kb-divisions__link"
                        >
                            <i class="kb-divisions__icon glyph-squares }}"></i>Все категории</a>
                    </li>
                </ul>
            </div>
            <div class="kb-line"></div>




            <div id="all-categories" class="kb-categories-map">
                <ul class="kb-categories-map__container">
                    <li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Домашний мастер" src="/images/966d51a2-d80f-41cb-ae35-07655cb4777f.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/domashniy-master"

                                                class="kb-iconed-category-list__name">Домашний мастер</a><span class="kb-iconed-category-list__count">27438</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/santehnik"

                                                class="kb-iconed-category-list__link">Сантехник</a><span class="kb-iconed-category-list__count">2573</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/elektrik"

                                                class="kb-iconed-category-list__link">Электрик</a><span class="kb-iconed-category-list__count">2859</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/muzh-na-chas"

                                                class="kb-iconed-category-list__link">Муж на час</a><span class="kb-iconed-category-list__count">7780</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-stolyara"

                                                class="kb-iconed-category-list__link">Столяр</a><span class="kb-iconed-category-list__count">7604</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-bytovoj-tehniki"

                                                class="kb-iconed-category-list__link">Установка бытовой техники</a><span class="kb-iconed-category-list__count">3074</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-24">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-24"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-uslugi-mastera"

                                                class="kb-iconed-category-list__link">Другие услуги мастера</a><span class="kb-iconed-category-list__count">3548</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Отделочные работы" src="/images/4d4b1d4c-4785-4569-8549-e862b93427fe.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/otdelochnye-raboty"

                                                class="kb-iconed-category-list__name">Отделочные работы</a><span class="kb-iconed-category-list__count">34468</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-kvartir"

                                                class="kb-iconed-category-list__link">Ремонт квартир</a><span class="kb-iconed-category-list__count">2580</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ukladka-plitki"

                                                class="kb-iconed-category-list__link">Укладка плитки</a><span class="kb-iconed-category-list__count">1462</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/shtukaturnie-raboti"

                                                class="kb-iconed-category-list__link">Штукатурные работы</a><span class="kb-iconed-category-list__count">1601</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/gipsokartonnie-raboti"

                                                class="kb-iconed-category-list__link">Монтаж гипсокартона</a><span class="kb-iconed-category-list__count">1859</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/malyarnie-raboti"

                                                class="kb-iconed-category-list__link">Малярные работы</a><span class="kb-iconed-category-list__count">2056</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-1081">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-1081"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/pokleyka-oboev"

                                                class="kb-iconed-category-list__link">Поклейка обоев</a><span class="kb-iconed-category-list__count">1874</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/napolnie-raboti"

                                                class="kb-iconed-category-list__link">Напольные работы</a><span class="kb-iconed-category-list__count">1817</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-dverey"

                                                class="kb-iconed-category-list__link">Установка и ремонт дверей</a><span class="kb-iconed-category-list__count">1484</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/chistka-mramora"

                                                class="kb-iconed-category-list__link">Полировка мрамора</a><span class="kb-iconed-category-list__count">340</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-okon"

                                                class="kb-iconed-category-list__link">Установка и ремонт окон</a><span class="kb-iconed-category-list__count">1042</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/demontazhnie-raboti"

                                                class="kb-iconed-category-list__link">Демонтажные работы</a><span class="kb-iconed-category-list__count">2878</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/fasadnye-raboty"

                                                class="kb-iconed-category-list__link">Фасадные работы</a><span class="kb-iconed-category-list__count">983</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/potolochnye-raboty"

                                                class="kb-iconed-category-list__link">Потолочные работы</a><span class="kb-iconed-category-list__count">2371</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugoe-v-ustanovke-okon"

                                                class="kb-iconed-category-list__link">Работа со стеклом</a><span class="kb-iconed-category-list__count">1563</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/shumoizolyaciya"

                                                class="kb-iconed-category-list__link">Звукоизоляция</a><span class="kb-iconed-category-list__count">3119</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/montazh-vagonki"

                                                class="kb-iconed-category-list__link">Монтаж вагонки</a><span class="kb-iconed-category-list__count">2415</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kapitalnyj"

                                                class="kb-iconed-category-list__link">Другие ремонтные работы</a><span class="kb-iconed-category-list__count">5024</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Клининговые услуги" src="/images/e8db6070-e92e-412e-861b-ab9a58a28940.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kliningovie-uslugi"

                                                class="kb-iconed-category-list__name">Клининговые услуги</a><span class="kb-iconed-category-list__count">30698</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-kvartir"

                                                class="kb-iconed-category-list__link">Уборка квартир</a><span class="kb-iconed-category-list__count">5286</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/generalnaya-uborka"

                                                class="kb-iconed-category-list__link">Генеральная уборка</a><span class="kb-iconed-category-list__count">3250</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-posle-remonta"

                                                class="kb-iconed-category-list__link">Уборка после ремонта</a><span class="kb-iconed-category-list__count">3404</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-ofisov"

                                                class="kb-iconed-category-list__link">Уборка офисов</a><span class="kb-iconed-category-list__count">3211</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/moyka-okon"

                                                class="kb-iconed-category-list__link">Мойка окон</a><span class="kb-iconed-category-list__count">3037</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-101">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-101"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/himchistka"

                                                class="kb-iconed-category-list__link">Химчистка</a><span class="kb-iconed-category-list__count">591</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-restoranov"

                                                class="kb-iconed-category-list__link">Уборка ресторанов</a><span class="kb-iconed-category-list__count">1273</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dezinsekciya"

                                                class="kb-iconed-category-list__link">Дезинсекция</a><span class="kb-iconed-category-list__count">234</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-kottedzhej-i-domov"

                                                class="kb-iconed-category-list__link">Уборка коттеджей и домов</a><span class="kb-iconed-category-list__count">2185</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/eko-uborka"

                                                class="kb-iconed-category-list__link">Эко-уборка</a><span class="kb-iconed-category-list__count">586</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-magazinov"

                                                class="kb-iconed-category-list__link">Уборка магазинов</a><span class="kb-iconed-category-list__count">1545</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-prilegayushih-territorij"

                                                class="kb-iconed-category-list__link">Уборка прилегающих территорий</a><span class="kb-iconed-category-list__count">1176</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dezinfekciya-pomeshenij"

                                                class="kb-iconed-category-list__link">Дезинфекция помещений</a><span class="kb-iconed-category-list__count">276</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/mojka-fasadov"

                                                class="kb-iconed-category-list__link">Мойка фасадов</a><span class="kb-iconed-category-list__count">613</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/deratizaciya"

                                                class="kb-iconed-category-list__link">Дератизация</a><span class="kb-iconed-category-list__count">188</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-posle-potopov-i-pozharov"

                                                class="kb-iconed-category-list__link">Уборка после потопов и пожаров</a><span class="kb-iconed-category-list__count">626</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uborka-proizvodstvennyh-pomeshenij"

                                                class="kb-iconed-category-list__link">Уборка производственных помещений</a><span class="kb-iconed-category-list__count">901</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugaya-uborka"

                                                class="kb-iconed-category-list__link">Другая уборка</a><span class="kb-iconed-category-list__count">2316</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Курьерские услуги" src="/images/efd2865d-91ba-4530-a9d6-d8ba28ce3971.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kurerskaya-sluzhba"

                                                class="kb-iconed-category-list__name">Курьерские услуги</a><span class="kb-iconed-category-list__count">122487</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kurerskaya-dostavka"

                                                class="kb-iconed-category-list__link">Курьерская доставка</a><span class="kb-iconed-category-list__count">11839</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-dokumentov"

                                                class="kb-iconed-category-list__link">Доставка документов</a><span class="kb-iconed-category-list__count">11039</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kurer-na-avto"

                                                class="kb-iconed-category-list__link">Курьер на авто</a><span class="kb-iconed-category-list__count">2792</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-podarkov"

                                                class="kb-iconed-category-list__link">Доставка подарков</a><span class="kb-iconed-category-list__count">11445</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-tovara-iz-internet-magazina"

                                                class="kb-iconed-category-list__link">Доставка товара из интернет-магазина</a><span class="kb-iconed-category-list__count">9951</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-1">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-1"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-produktov"

                                                class="kb-iconed-category-list__link">Доставка продуктов и готовой еды</a><span class="kb-iconed-category-list__count">10411</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-lekarstv"

                                                class="kb-iconed-category-list__link">Доставка лекарств</a><span class="kb-iconed-category-list__count">10100</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dostavka-tsvetov"

                                                class="kb-iconed-category-list__link">Доставка цветов</a><span class="kb-iconed-category-list__count">10882</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/otpravit-poezdom-ili-marshrutkoj"

                                                class="kb-iconed-category-list__link">Отправить поездом или маршруткой</a><span class="kb-iconed-category-list__count">11530</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/zabrat-i-otpravit-pochtoj"

                                                class="kb-iconed-category-list__link">Забрать и отправить почтой</a><span class="kb-iconed-category-list__count">10156</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/nayti"

                                                class="kb-iconed-category-list__link">Найти</a><span class="kb-iconed-category-list__count">5743</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/postoyat-v-ocheredi"

                                                class="kb-iconed-category-list__link">Постоять в очереди</a><span class="kb-iconed-category-list__count">6071</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-kurerskie-uslugi"

                                                class="kb-iconed-category-list__link">Другие курьерские услуги</a><span class="kb-iconed-category-list__count">10528</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Строительные работы" src="/images/e8856d70-45b1-401f-9bcd-1b4c6c56c825.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/stroitelnie-raboti"

                                                class="kb-iconed-category-list__name">Строительные работы</a><span class="kb-iconed-category-list__count">40057</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/raznorabochie"

                                                class="kb-iconed-category-list__link">Разнорабочие</a><span class="kb-iconed-category-list__count">3375</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/svarochnye-raboty"

                                                class="kb-iconed-category-list__link">Сварочные работы</a><span class="kb-iconed-category-list__count">982</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/tokarnye-raboty"

                                                class="kb-iconed-category-list__link">Токарные работы</a><span class="kb-iconed-category-list__count">195</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-plotnika"

                                                class="kb-iconed-category-list__link">Плотник</a><span class="kb-iconed-category-list__count">3141</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kladka-kirpicha"

                                                class="kb-iconed-category-list__link">Кладка кирпича</a><span class="kb-iconed-category-list__count">828</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-108">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-108"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-zaborov-i-vorot"

                                                class="kb-iconed-category-list__link">Установка заборов и ворот</a><span class="kb-iconed-category-list__count">901</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/krovelnye-raboty"

                                                class="kb-iconed-category-list__link">Кровельные работы</a><span class="kb-iconed-category-list__count">701</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/betonnye-raboty"

                                                class="kb-iconed-category-list__link">Бетонные работы</a><span class="kb-iconed-category-list__count">925</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/burenie-skvazhin-i-kolodcev"

                                                class="kb-iconed-category-list__link">Буровые работы</a><span class="kb-iconed-category-list__count">559</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/fundamentnye-raboty"

                                                class="kb-iconed-category-list__link">Фундаментные работы</a><span class="kb-iconed-category-list__count">861</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/stroitelstvo-domov"

                                                class="kb-iconed-category-list__link">Строительство домов</a><span class="kb-iconed-category-list__count">912</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/stroitelstvo-melkih-konstrukcij"

                                                class="kb-iconed-category-list__link">Строительство мелких конструкций</a><span class="kb-iconed-category-list__count">4656</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/landshaftnye-raboty"

                                                class="kb-iconed-category-list__link">Ландшафтные работы</a><span class="kb-iconed-category-list__count">828</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/arhitektura-i-proektirovanie"

                                                class="kb-iconed-category-list__link">Архитектура и проектирование</a><span class="kb-iconed-category-list__count">1699</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sostavlenie-smet"

                                                class="kb-iconed-category-list__link">Составление смет</a><span class="kb-iconed-category-list__count">3615</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/zemlyanye-raboty"

                                                class="kb-iconed-category-list__link">Земляные работы</a><span class="kb-iconed-category-list__count">2733</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/montazh-ventilyacii"

                                                class="kb-iconed-category-list__link">Монтаж вентиляции</a><span class="kb-iconed-category-list__count">5019</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/promyshlennyj-alpinizm"

                                                class="kb-iconed-category-list__link">Промышленный альпинизм</a><span class="kb-iconed-category-list__count">304</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-stroyraboti"

                                                class="kb-iconed-category-list__link">Другие стройработы</a><span class="kb-iconed-category-list__count">7823</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Ремонт техники" src="/images/77847788-0c75-4b27-8f34-8107493a1bd3.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-tehniki"

                                                class="kb-iconed-category-list__name">Ремонт техники</a><span class="kb-iconed-category-list__count">27053</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-bitovoy-tehniki"

                                                class="kb-iconed-category-list__link">Ремонт крупной бытовой техники</a><span class="kb-iconed-category-list__count">2053</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-melkoj-bytovoj-tehniki"

                                                class="kb-iconed-category-list__link">Ремонт мелкой бытовой техники</a><span class="kb-iconed-category-list__count">2233</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kompyuternaya-pomosh"

                                                class="kb-iconed-category-list__link">Компьютерная помощь</a><span class="kb-iconed-category-list__count">2971</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-tsifrovoy-tehniki"

                                                class="kb-iconed-category-list__link">Ремонт цифровой техники</a><span class="kb-iconed-category-list__count">1958</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/obsluzhivanie-tehniki"

                                                class="kb-iconed-category-list__link">Обслуживание техники</a><span class="kb-iconed-category-list__count">2444</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-125">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-125"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-orgtehniki"

                                                class="kb-iconed-category-list__link">Ремонт оргтехники</a><span class="kb-iconed-category-list__count">1839</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-tehnika-dlya-krasoty-i-uhoda"

                                                class="kb-iconed-category-list__link">Ремонт техники для красоты и ухода</a><span class="kb-iconed-category-list__count">3339</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-elektroinstrumentov"

                                                class="kb-iconed-category-list__link">Ремонт электроинструментов </a><span class="kb-iconed-category-list__count">996</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-medtehniki"

                                                class="kb-iconed-category-list__link">Ремонт медтехники</a><span class="kb-iconed-category-list__count">3335</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-sadovo-parkovoj-tehniki"

                                                class="kb-iconed-category-list__link">Ремонт садово-парковой техники</a><span class="kb-iconed-category-list__count">844</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-i-remont-sistem-bezopasnosti"

                                                class="kb-iconed-category-list__link">Установка и ремонт систем безопасности</a><span class="kb-iconed-category-list__count">1924</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugoy-remont-tehniki"

                                                class="kb-iconed-category-list__link">Другой ремонт техники</a><span class="kb-iconed-category-list__count">3117</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Логистические и складские услуги" src="/images/abe03f5c-8534-4707-9b4b-034e000466d6.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/logisticheskie-i-skladskie-uslugi"

                                                class="kb-iconed-category-list__name">Логистические и складские услуги</a><span class="kb-iconed-category-list__count">19904</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/gruzovie-perevozki"

                                                class="kb-iconed-category-list__link">Грузоперевозки</a><span class="kb-iconed-category-list__count">1861</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-gruzchikov"

                                                class="kb-iconed-category-list__link">Услуги грузчиков</a><span class="kb-iconed-category-list__count">3380</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vivoz-stroitelnogo-musora"

                                                class="kb-iconed-category-list__link">Вывоз строительного мусора</a><span class="kb-iconed-category-list__count">1334</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/perevozka-mebeli-i-tehniki"

                                                class="kb-iconed-category-list__link">Перевозка мебели и техники</a><span class="kb-iconed-category-list__count">1342</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/pereezd-kvartiry-ili-ofisa"

                                                class="kb-iconed-category-list__link">Переезд квартиры или офиса</a><span class="kb-iconed-category-list__count">1505</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-192">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-192"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-taksi"

                                                class="kb-iconed-category-list__link">Услуги такси</a><span class="kb-iconed-category-list__count">1449</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/trezvyj-voditel"

                                                class="kb-iconed-category-list__link">Трезвый водитель</a><span class="kb-iconed-category-list__count">145</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/passazhirskie-perevozki"

                                                class="kb-iconed-category-list__link">Пассажирские перевозки</a><span class="kb-iconed-category-list__count">1482</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/mezhdugorodnyaya-perevozka"

                                                class="kb-iconed-category-list__link">Междугородняя перевозка</a><span class="kb-iconed-category-list__count">923</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/upakovka-i-sortirovka"

                                                class="kb-iconed-category-list__link">Упаковка и сортировка</a><span class="kb-iconed-category-list__count">2386</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-sklada"

                                                class="kb-iconed-category-list__link">Услуги склада</a><span class="kb-iconed-category-list__count">719</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/mezhdunarodnaya-perevozka"

                                                class="kb-iconed-category-list__link">Международная перевозка</a><span class="kb-iconed-category-list__count">395</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vyvoz-snega-i-musora"

                                                class="kb-iconed-category-list__link">Вывоз снега</a><span class="kb-iconed-category-list__count">478</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/tamozhennoe-oformlenie"

                                                class="kb-iconed-category-list__link">Таможенное оформление</a><span class="kb-iconed-category-list__count">228</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-ekspeditora"

                                                class="kb-iconed-category-list__link">Услуги экспедитора</a><span class="kb-iconed-category-list__count">1129</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-logisticheskie-i-skladskie-uslugi"

                                                class="kb-iconed-category-list__link">Другие логистические и складские услуги</a><span class="kb-iconed-category-list__count">1148</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Бытовые услуги" src="/images/b511ae0a-b379-4b87-a53f-1ff41f42bb2c.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/bitovie-uslugi"

                                                class="kb-iconed-category-list__name">Бытовые услуги</a><span class="kb-iconed-category-list__count">20841</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/nyanya-dlya-rebenka"

                                                class="kb-iconed-category-list__link">Няни</a><span class="kb-iconed-category-list__count">4081</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-sidelki"

                                                class="kb-iconed-category-list__link">Услуги сиделки</a><span class="kb-iconed-category-list__count">2175</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-shvei"

                                                class="kb-iconed-category-list__link">Услуги швеи</a><span class="kb-iconed-category-list__count">813</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-obuvi"

                                                class="kb-iconed-category-list__link">Ремонт обуви</a><span class="kb-iconed-category-list__count">248</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-povara"

                                                class="kb-iconed-category-list__link">Услуги повара</a><span class="kb-iconed-category-list__count">1551</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-16">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-16"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/stirka-glazhka"

                                                class="kb-iconed-category-list__link">Стирка и глажка вещей</a><span class="kb-iconed-category-list__count">2229</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-sumok"

                                                class="kb-iconed-category-list__link">Ремонт сумок</a><span class="kb-iconed-category-list__count">229</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/pokraska-odezhdy"

                                                class="kb-iconed-category-list__link">Покраска одежды</a><span class="kb-iconed-category-list__count">57</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sad-i-ogorod"

                                                class="kb-iconed-category-list__link">Сад и огород</a><span class="kb-iconed-category-list__count">2723</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-velosipedov"

                                                class="kb-iconed-category-list__link">Ремонт велосипедов</a><span class="kb-iconed-category-list__count">826</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/zatochka-instrumentov"

                                                class="kb-iconed-category-list__link">Заточка инструментов</a><span class="kb-iconed-category-list__count">2502</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-yuvelirnyh-izdelij"

                                                class="kb-iconed-category-list__link">Ремонт ювелирных изделий</a><span class="kb-iconed-category-list__count">139</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-chasov"

                                                class="kb-iconed-category-list__link">Ремонт часов</a><span class="kb-iconed-category-list__count">133</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-bitovie-uslugi"

                                                class="kb-iconed-category-list__link">Другие бытовые услуги</a><span class="kb-iconed-category-list__count">3135</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Мебельные работы" src="/images/fd37cd88-3853-45b0-b02e-b003f2fbbe75.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/mebelnye-raboty"

                                                class="kb-iconed-category-list__name">Мебельные работы</a><span class="kb-iconed-category-list__count">20597</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/izgotovlenie-mebeli"

                                                class="kb-iconed-category-list__link">Изготовление мебели</a><span class="kb-iconed-category-list__count">3281</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-mebeli"

                                                class="kb-iconed-category-list__link">Ремонт мебели</a><span class="kb-iconed-category-list__count">3318</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sborka-mebeli"

                                                class="kb-iconed-category-list__link">Сборка мебели</a><span class="kb-iconed-category-list__count">3477</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/restavraciya-mebeli"

                                                class="kb-iconed-category-list__link">Реставрация мебели</a><span class="kb-iconed-category-list__count">3217</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/peretyazhka-myagkoj-mebeli"

                                                class="kb-iconed-category-list__link">Перетяжка мебели</a><span class="kb-iconed-category-list__count">3179</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-1199">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-1199"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/shveya-myagkoj-mebeli"

                                                class="kb-iconed-category-list__link">Швея мягкой мебели</a><span class="kb-iconed-category-list__count">776</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-mebelnye-raboty"

                                                class="kb-iconed-category-list__link">Другие мебельные работы</a><span class="kb-iconed-category-list__count">3349</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Работа в Интернете" src="/images/4a1dcb3a-32d5-41d5-80ba-cfff2e43b364.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/rabota-v-internete"

                                                class="kb-iconed-category-list__name">Работа в Интернете</a><span class="kb-iconed-category-list__count">64447</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kopirayting"

                                                class="kb-iconed-category-list__link">Копирайтинг</a><span class="kb-iconed-category-list__count">6181</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sbor-poisk-informatsii"

                                                class="kb-iconed-category-list__link">Сбор, поиск информации</a><span class="kb-iconed-category-list__count">8170</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/napolnenie-saytov"

                                                class="kb-iconed-category-list__link">Наполнение сайтов</a><span class="kb-iconed-category-list__count">5112</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/nabor-teksta"

                                                class="kb-iconed-category-list__link">Набор текста</a><span class="kb-iconed-category-list__count">12106</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/rerayt"

                                                class="kb-iconed-category-list__link">Рерайтинг</a><span class="kb-iconed-category-list__count">4809</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-26">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-26"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vvod-dannih"

                                                class="kb-iconed-category-list__link">Ввод данных</a><span class="kb-iconed-category-list__count">9031</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/rasshifrovka-intervyu"

                                                class="kb-iconed-category-list__link">Расшифровка интервью</a><span class="kb-iconed-category-list__count">3716</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sozdanie-prezentatsiy"

                                                class="kb-iconed-category-list__link">Создание презентаций</a><span class="kb-iconed-category-list__count">5526</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ozvuchivanie-igr"

                                                class="kb-iconed-category-list__link">Дикторское озвучивание</a><span class="kb-iconed-category-list__count">1094</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugaya-onlayn-rabota"

                                                class="kb-iconed-category-list__link">Другая онлайн работа</a><span class="kb-iconed-category-list__count">8702</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Дизайн" src="/images/d42f4631-04e3-4a59-9e97-601bf6eedbca.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dizayner"

                                                class="kb-iconed-category-list__name">Дизайн</a><span class="kb-iconed-category-list__count">16771</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razrabotka-logotipov"

                                                class="kb-iconed-category-list__link">Разработка логотипов</a><span class="kb-iconed-category-list__count">2832</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dizajn-interera"

                                                class="kb-iconed-category-list__link">Дизайн интерьера</a><span class="kb-iconed-category-list__count">1927</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dizajn-sajta"

                                                class="kb-iconed-category-list__link">Дизайн сайта</a><span class="kb-iconed-category-list__count">1651</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dizajn-poligrafii"

                                                class="kb-iconed-category-list__link">Дизайн полиграфии</a><span class="kb-iconed-category-list__count">2463</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/risunki-i-illyustracii"

                                                class="kb-iconed-category-list__link">Рисунки и иллюстрации</a><span class="kb-iconed-category-list__count">1218</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-45">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-45"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dizajn-bannerov"

                                                class="kb-iconed-category-list__link">Дизайн баннеров</a><span class="kb-iconed-category-list__count">2531</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/3d-dizajn"

                                                class="kb-iconed-category-list__link">3D дизайн</a><span class="kb-iconed-category-list__count">1737</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razrabotka-infografiki"

                                                class="kb-iconed-category-list__link">Разработка инфографики</a><span class="kb-iconed-category-list__count">650</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uxui-dizajn"

                                                class="kb-iconed-category-list__link">UX/UI дизайн</a><span class="kb-iconed-category-list__count">390</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugoe-v-dizajne"

                                                class="kb-iconed-category-list__link">Другое в дизайне</a><span class="kb-iconed-category-list__count">1372</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Разработка сайтов и приложений" src="/images/162b0837-1a08-4afb-932c-4624357f0f20.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razrabotka-saytov"

                                                class="kb-iconed-category-list__name">Разработка сайтов и приложений</a><span class="kb-iconed-category-list__count">17068</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sozdanie-saytov"

                                                class="kb-iconed-category-list__link">Создание сайтов</a><span class="kb-iconed-category-list__count">2066</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dorabotki-po-saytu"

                                                class="kb-iconed-category-list__link">Доработка сайта</a><span class="kb-iconed-category-list__count">1670</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/landing-page"

                                                class="kb-iconed-category-list__link">Создание Landing page</a><span class="kb-iconed-category-list__count">2039</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/prodvizhenie-saytov"

                                                class="kb-iconed-category-list__link">Продвижение сайтов</a><span class="kb-iconed-category-list__count">4278</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/verstka-sayta"

                                                class="kb-iconed-category-list__link">Верстка сайта</a><span class="kb-iconed-category-list__count">836</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-92">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-92"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/testirovanie-igr"

                                                class="kb-iconed-category-list__link">Тестирование ПО (QA)</a><span class="kb-iconed-category-list__count">431</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vnedrenie-1c"

                                                class="kb-iconed-category-list__link">Внедрение 1C</a><span class="kb-iconed-category-list__count">190</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-v-sfere-it"

                                                class="kb-iconed-category-list__link">Услуги в сфере IT</a><span class="kb-iconed-category-list__count">2590</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razrabotka-mobilnyh-prilozhenij"

                                                class="kb-iconed-category-list__link">Разработка мобильных приложений</a><span class="kb-iconed-category-list__count">593</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razrabotka-igr"

                                                class="kb-iconed-category-list__link">Разработка игр</a><span class="kb-iconed-category-list__count">463</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-raboti-po-saytu"

                                                class="kb-iconed-category-list__link">Другие работы по разработке сайтов</a><span class="kb-iconed-category-list__count">1912</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Деловые услуги" src="/images/f01991f5-5aeb-4d0d-b165-888b3a4bdad7.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/delovye-uslugi"

                                                class="kb-iconed-category-list__name">Деловые услуги</a><span class="kb-iconed-category-list__count">8915</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/buhgalterskie-uslugi"

                                                class="kb-iconed-category-list__link">Бухгалтерские услуги</a><span class="kb-iconed-category-list__count">935</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/yuridicheskie-uslugi"

                                                class="kb-iconed-category-list__link">Юридические услуги</a><span class="kb-iconed-category-list__count">1046</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/brokerskie-i-agentskie-uslugi"

                                                class="kb-iconed-category-list__link">Риэлтор</a><span class="kb-iconed-category-list__count">585</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/call-center"

                                                class="kb-iconed-category-list__link">Услуги колл-центра</a><span class="kb-iconed-category-list__count">952</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/finansovye-uslugi"

                                                class="kb-iconed-category-list__link">Финансовые услуги</a><span class="kb-iconed-category-list__count">760</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-769">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-769"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/konsaltingovye-uslugi"

                                                class="kb-iconed-category-list__link">Консалтинговые услуги</a><span class="kb-iconed-category-list__count">956</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kadrovye-uslugi"

                                                class="kb-iconed-category-list__link">Кадровые услуги</a><span class="kb-iconed-category-list__count">892</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-bezopasnosti"

                                                class="kb-iconed-category-list__link">Услуги безопасности</a><span class="kb-iconed-category-list__count">664</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-v-sfere-migracii"

                                                class="kb-iconed-category-list__link">Услуги в сфере миграции</a><span class="kb-iconed-category-list__count">328</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-delovye-uslugi"

                                                class="kb-iconed-category-list__link">Другие деловые услуги</a><span class="kb-iconed-category-list__count">1797</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Реклама в Интернете" src="/images/9368c34f-d675-4c95-9123-446c85afd49d.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/reklama-v-internete"

                                                class="kb-iconed-category-list__name">Реклама в Интернете</a><span class="kb-iconed-category-list__count">5790</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/reklama-v-socialnyh-setyah"

                                                class="kb-iconed-category-list__link">Реклама в социальных сетях</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razmeshchenie-obyavleniy"

                                                class="kb-iconed-category-list__link">Размещение объявлений</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/nastrojka-kontekstnoj-kampanii"

                                                class="kb-iconed-category-list__link">Настройка контекстной рекламы</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/seo-optimizaciya-sajta"

                                                class="kb-iconed-category-list__link">SEO оптимизация сайта</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razmeshenie-postov-na-forumah"

                                                class="kb-iconed-category-list__link">Размещение постов на форумах</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-387">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-387"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/seo-audit-sajta"

                                                class="kb-iconed-category-list__link">SEO аудит сайта</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/email-marketing"

                                                class="kb-iconed-category-list__link">Email-маркетинг</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugaya-reklama-v-internete"

                                                class="kb-iconed-category-list__link">Другая реклама в Интернете</a><span class="kb-iconed-category-list__count">5790</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Распространение рекламы" src="/images/1a0eb957-a77a-49ef-a071-9b24c9e24601.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razdacha-listovok"

                                                class="kb-iconed-category-list__name">Распространение рекламы</a><span class="kb-iconed-category-list__count">5061</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/razdacha-flaerov"

                                                class="kb-iconed-category-list__link">Раздача флаеров</a><span class="kb-iconed-category-list__count">5061</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/raskleyka-obyavleniy"

                                                class="kb-iconed-category-list__link">Расклейка объявлений</a><span class="kb-iconed-category-list__count">5061</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/reklama-v-pochtovie-yashchiki"

                                                class="kb-iconed-category-list__link">Реклама в почтовые ящики</a><span class="kb-iconed-category-list__count">5061</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/sendvich-panel"

                                                class="kb-iconed-category-list__link">Реклама в сэндвич-панеле</a><span class="kb-iconed-category-list__count">5061</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugoe-v-rasprostranenie-reklamy"

                                                class="kb-iconed-category-list__link">Другое в распространение рекламы</a><span class="kb-iconed-category-list__count">5061</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-17">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-17"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">

                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Ремонт авто" src="/images/e7d42fd4-81cc-4cdf-8021-2512092d248e.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/avtouslugi"

                                                class="kb-iconed-category-list__name">Ремонт авто</a><span class="kb-iconed-category-list__count">6054</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/pomosh-v-doroge"

                                                class="kb-iconed-category-list__link">Помощь в дороге</a><span class="kb-iconed-category-list__count">1223</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/tehnicheskoe-obsluzhivanie-to"

                                                class="kb-iconed-category-list__link">Техническое обслуживание и диагностика</a><span class="kb-iconed-category-list__count">310</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/avtoelektrika"

                                                class="kb-iconed-category-list__link">Автоэлектрика</a><span class="kb-iconed-category-list__count">283</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kuzovnye-raboty"

                                                class="kb-iconed-category-list__link">Кузовные работы</a><span class="kb-iconed-category-list__count">238</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dvigatel"

                                                class="kb-iconed-category-list__link">Двигатель</a><span class="kb-iconed-category-list__count">238</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-789">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-789"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-transmissii-akpp-mkpp"

                                                class="kb-iconed-category-list__link">Коробка переключения передач</a><span class="kb-iconed-category-list__count">129</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-salonom"

                                                class="kb-iconed-category-list__link">Уход за салоном</a><span class="kb-iconed-category-list__count">610</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-i-zamena-avtostekol"

                                                class="kb-iconed-category-list__link">Ремонт и замена автостекол</a><span class="kb-iconed-category-list__count">115</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/diagnostika-i-remont-hodovoj"

                                                class="kb-iconed-category-list__link">Ходовая часть</a><span class="kb-iconed-category-list__count">258</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/rulevaya-sistema"

                                                class="kb-iconed-category-list__link">Рулевая система</a><span class="kb-iconed-category-list__count">217</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-tormoznoj-sistemy"

                                                class="kb-iconed-category-list__link">Тормозная система</a><span class="kb-iconed-category-list__count">253</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-vyhlopnyh-sistem"

                                                class="kb-iconed-category-list__link">Выхлопная система</a><span class="kb-iconed-category-list__count">181</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/shinomontazh-i-balansirovka"

                                                class="kb-iconed-category-list__link">Шиномонтаж</a><span class="kb-iconed-category-list__count">160</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/diagnostika-i-remont-avtokondicionera"

                                                class="kb-iconed-category-list__link">Ремонт и обслуживание автокондиционеров</a><span class="kb-iconed-category-list__count">95</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/ustanovka-gazobalonnogo-oborudovaniya-gbo"

                                                class="kb-iconed-category-list__link">Установка газобаллонного оборудования (ГБО)</a><span class="kb-iconed-category-list__count">129</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/remont-mototehniki"

                                                class="kb-iconed-category-list__link">Ремонт мототехники</a><span class="kb-iconed-category-list__count">828</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/avtostrahovanie"

                                                class="kb-iconed-category-list__link">Автострахование</a><span class="kb-iconed-category-list__count">90</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-avtouslugi"

                                                class="kb-iconed-category-list__link">Другой ремонт авто</a><span class="kb-iconed-category-list__count">697</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Услуги для животных" src="/images/94186c7b-b0a0-4272-8d18-4cf61a1750b1.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-zhivotnimi"

                                                class="kb-iconed-category-list__name">Услуги для животных</a><span class="kb-iconed-category-list__count">23286</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-kotami"

                                                class="kb-iconed-category-list__link">Уход за котами</a><span class="kb-iconed-category-list__count">4497</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-sobakami"

                                                class="kb-iconed-category-list__link">Уход за собаками</a><span class="kb-iconed-category-list__count">4858</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/gostinica-dlya-zhivotnyh"

                                                class="kb-iconed-category-list__link">Гостиница для животных</a><span class="kb-iconed-category-list__count">1478</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/perevozka-zhivotnyh"

                                                class="kb-iconed-category-list__link">Перевозка животных</a><span class="kb-iconed-category-list__count">3202</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-rybkami"

                                                class="kb-iconed-category-list__link">Уход за рыбками</a><span class="kb-iconed-category-list__count">2000</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-9">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-9"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-gryzunami"

                                                class="kb-iconed-category-list__link">Уход за грызунами</a><span class="kb-iconed-category-list__count">1913</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-pticami"

                                                class="kb-iconed-category-list__link">Уход за птицами</a><span class="kb-iconed-category-list__count">1616</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-reptiliyami"

                                                class="kb-iconed-category-list__link">Уход за рептилиями</a><span class="kb-iconed-category-list__count">960</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-zoouslugi"

                                                class="kb-iconed-category-list__link">Другие зооуслуги</a><span class="kb-iconed-category-list__count">2762</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Услуги красоты и здоровья" src="/images/c4f84cfb-356b-48e0-acfa-1bc14613358c.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/krasota-i-zdorove"

                                                class="kb-iconed-category-list__name">Услуги красоты и здоровья</a><span class="kb-iconed-category-list__count">5609</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/massazh"

                                                class="kb-iconed-category-list__link">Массаж</a><span class="kb-iconed-category-list__count">925</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-nogtyami"

                                                class="kb-iconed-category-list__link">Уход за ногтями</a><span class="kb-iconed-category-list__count">646</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/kosmetologiya"

                                                class="kb-iconed-category-list__link">Косметология</a><span class="kb-iconed-category-list__count">314</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-resnicami"

                                                class="kb-iconed-category-list__link">Уход за ресницами</a><span class="kb-iconed-category-list__count">290</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uhod-za-brovyami"

                                                class="kb-iconed-category-list__link">Уход за бровями</a><span class="kb-iconed-category-list__count">426</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-79">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-79"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/parikmaherskie-uslugi"

                                                class="kb-iconed-category-list__link">Парикмахерские услуги</a><span class="kb-iconed-category-list__count">603</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/depilyatsiya"

                                                class="kb-iconed-category-list__link">Депиляция</a><span class="kb-iconed-category-list__count">328</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vizazhist"

                                                class="kb-iconed-category-list__link">Услуги визажиста-стилиста</a><span class="kb-iconed-category-list__count">452</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/makiyazh"

                                                class="kb-iconed-category-list__link">Макияж</a><span class="kb-iconed-category-list__count">377</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-psihologa"

                                                class="kb-iconed-category-list__link">Психолог</a><span class="kb-iconed-category-list__count">352</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/uslugi-dietologa"

                                                class="kb-iconed-category-list__link">Диетолог</a><span class="kb-iconed-category-list__count">91</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugoy-uhod-za-soboy"

                                                class="kb-iconed-category-list__link">Другой уход за собой</a><span class="kb-iconed-category-list__count">805</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Услуги репетиторов" src="/images/9fb0ca76-0da8-4bb2-8c02-42da98214c76.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/repetitori"

                                                class="kb-iconed-category-list__name">Услуги репетиторов</a><span class="kb-iconed-category-list__count">44907</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/prepodavateli-po-predmetam"

                                                class="kb-iconed-category-list__link">Преподаватели по предметам</a><span class="kb-iconed-category-list__count">10872</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/repetitory-inostrannyh-yazykov"

                                                class="kb-iconed-category-list__link">Репетиторы иностранных языков</a><span class="kb-iconed-category-list__count">8544</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/obrazovanie"

                                                class="kb-iconed-category-list__link">Написание работ</a><span class="kb-iconed-category-list__count">9817</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/byuro-perevodov"

                                                class="kb-iconed-category-list__link">Бюро переводов</a><span class="kb-iconed-category-list__count">9972</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/prepodavateli-muzyki"

                                                class="kb-iconed-category-list__link">Преподаватели музыки</a><span class="kb-iconed-category-list__count">2314</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-52">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-52"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/instruktor-po-vozhdeniyu"

                                                class="kb-iconed-category-list__link">Автоинструкторы</a><span class="kb-iconed-category-list__count">2816</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-repetitori"

                                                class="kb-iconed-category-list__link">Другие репетиторы</a><span class="kb-iconed-category-list__count">572</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li><li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <img alt="Организация праздников" src="/images/3de9feed-6281-4793-8a1b-77b7c3178ede.png">
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/organizatsiya-prazdnikov"

                                                class="kb-iconed-category-list__name">Организация праздников</a><span class="kb-iconed-category-list__count">18448</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vedushie-i-zvezdy"

                                                class="kb-iconed-category-list__link">Услуги ведущего</a><span class="kb-iconed-category-list__count">791</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/fotograf"

                                                class="kb-iconed-category-list__link">Фотографы</a><span class="kb-iconed-category-list__count">2145</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/videooperator"

                                                class="kb-iconed-category-list__link">Видеооператоры</a><span class="kb-iconed-category-list__count">993</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/muzykalnoe-soprovozhdenie"

                                                class="kb-iconed-category-list__link">Музыкальное сопровождение</a><span class="kb-iconed-category-list__count">766</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/dlya-detej"

                                                class="kb-iconed-category-list__link">Услуги аниматоров</a><span class="kb-iconed-category-list__count">1225</span>
                                    </li>


                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a class="kb-iconed-category-list__link js-open-extra-138">Показать еще<i class="kb-iconed-category-list__arrow glyph-arrow-down"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul data-bazooka="IToggleable" data-trigger="js-open-extra-138"
                                    data-hide-trigger=""
                                    class="kb-iconed-category-list__sublist kb-iconed-category-list__sublist_type_toggleable kb-hidden">


                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/organizaciya-pitaniya"

                                                class="kb-iconed-category-list__link">Организация питания</a><span class="kb-iconed-category-list__count">771</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/vypechka-i-deserty"

                                                class="kb-iconed-category-list__link">Выпечка и десерты</a><span class="kb-iconed-category-list__count">1821</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/personal-dlya-meropriyatij"

                                                class="kb-iconed-category-list__link">Персонал для мероприятий</a><span class="kb-iconed-category-list__count">6203</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/oformlenie-pomeshenij"

                                                class="kb-iconed-category-list__link">Оформление праздников</a><span class="kb-iconed-category-list__count">1312</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/prazdnik-pod-klyuch"

                                                class="kb-iconed-category-list__link">Праздник под ключ</a><span class="kb-iconed-category-list__count">635</span>
                                    </li>



                                    <li class="kb-iconed-category-list__item">
                                        <a

                                                href="https://kiev.kabanchik.ua/category/drugie-ivent-uslugi"

                                                class="kb-iconed-category-list__link">Другие ивент услуги</a><span class="kb-iconed-category-list__count">1786</span>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="kb-categories-map__item-holder">
                        <div class="kb-categories-map__item">
                            <div class="kb-iconed-category-list">
                                <div class="kb-iconed-category-list__heading">
                                    <div class="kb-iconed-category-list__icon-holder">
                                        <i class="icon-squares_green"></i>
                                    </div>
                                    <div class="kb-iconed-category-list__name-holder">
                                        <span class="kb-iconed-category-list__name">Другие категории</span>
                                    </div>
                                </div>
                                <ul class="kb-iconed-category-list__sublist">
                                    <li class="kb-iconed-category-list__item">
                                        <a
                                                href="/home"
                                                class="kb-iconed-category-list__link">Дом</a>
                                    </li><li class="kb-iconed-category-list__item">
                                        <a
                                                href="/delivery"
                                                class="kb-iconed-category-list__link">Доставка</a>
                                    </li><li class="kb-iconed-category-list__item">
                                        <a
                                                href="/freelance"
                                                class="kb-iconed-category-list__link">Фриланс</a>
                                    </li><li class="kb-iconed-category-list__item">
                                        <a
                                                href="/teachers"
                                                class="kb-iconed-category-list__link">Преподаватели</a>
                                    </li><li class="kb-iconed-category-list__item">
                                        <a
                                                href="/business"
                                                class="kb-iconed-category-list__link">Бизнес</a>
                                    </li>
                                    <li class="kb-iconed-category-list__item kb-iconed-category-list__item_type_last">
                                        <a
                                                href="https://kiev.kabanchik.ua/all-categories"
                                                class="kb-iconed-category-list__link">Все категории услуг<i class="kb-iconed-category-list__arrow glyph-arrow-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="kb-landing-section kb-landing-section_type_mt-0">
                <div class="kb-landing-section__title">Не нашли нужную услугу? Используйте поиск!</div>
                <div class="kb-landing-section__search-holder">
                    <form data-bazooka="Searchbar" data-type=small action="https://kabanchik.ua/search" method="get" autocomplete="off">
                        <div class="b-search">
                            <button class="b-search__button">
                                <i class="b-search__button-icon glyph-search"></i>
                            </button>
                            <div class="b-search__dropdown">







                                <input class="js-search-dropdown-region" type="hidden" name="region" value="1"/>
                                <div class="js-search-dropdown-text b-search__dropdown-text b-text-ellipsis">Киев</div>
                                <ul class="b-search__dropdown-list">

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="24" class="b-search__suggest-link">Александрия</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="25" class="b-search__suggest-link">Белая Церковь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="67" class="b-search__suggest-link">Белгород-Днестровский</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="26" class="b-search__suggest-link">Бердичев</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="27" class="b-search__suggest-link">Бердянск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="28" class="b-search__suggest-link">Березань</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="29" class="b-search__suggest-link">Борисполь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="62" class="b-search__suggest-link">Боярка</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="30" class="b-search__suggest-link">Бровары</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="31" class="b-search__suggest-link">Буча</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="32" class="b-search__suggest-link">Васильков</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="5" class="b-search__suggest-link">Винница</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="61" class="b-search__suggest-link">Вишнёвое</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="33" class="b-search__suggest-link">Вышгород</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="2" class="b-search__suggest-link">Днепр (Днепропетровск)</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="63" class="b-search__suggest-link">Дрогобыч</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="12" class="b-search__suggest-link">Житомир</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="6" class="b-search__suggest-link">Запорожье</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="7" class="b-search__suggest-link">Ивано-Франковск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="35" class="b-search__suggest-link">Измаил</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="37" class="b-search__suggest-link">Ирпень</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="38" class="b-search__suggest-link">Каменец-Подольский</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="34" class="b-search__suggest-link">Каменское (Днепродзержинск)</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="1" class="b-search__suggest-link">Киев</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="58" class="b-search__suggest-link">Ковель</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="39" class="b-search__suggest-link">Конотоп</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="72" class="b-search__suggest-link">Краматорск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="40" class="b-search__suggest-link">Кременчуг</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="23" class="b-search__suggest-link">Кривой Рог</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="22" class="b-search__suggest-link">Кропивницкий (Кировоград)</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="41" class="b-search__suggest-link">Лисичанск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="68" class="b-search__suggest-link">Лозовая</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="17" class="b-search__suggest-link">Луцк</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="13" class="b-search__suggest-link">Львов</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="42" class="b-search__suggest-link">Мариуполь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="43" class="b-search__suggest-link">Мелитополь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="44" class="b-search__suggest-link">Мукачево</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="69" class="b-search__suggest-link">Нежин</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="10" class="b-search__suggest-link">Николаев</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="45" class="b-search__suggest-link">Никополь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="59" class="b-search__suggest-link">Новомосковск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="46" class="b-search__suggest-link">Обухов</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="4" class="b-search__suggest-link">Одесса</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="47" class="b-search__suggest-link">Павлоград</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="65" class="b-search__suggest-link">Первомайск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="48" class="b-search__suggest-link">Переяслав-Хмельницкий</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="18" class="b-search__suggest-link">Полтава</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="70" class="b-search__suggest-link">Прилуки</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="49" class="b-search__suggest-link">Ржищев</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="21" class="b-search__suggest-link">Ровно</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="50" class="b-search__suggest-link">Северодонецк</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="51" class="b-search__suggest-link">Славутич</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="52" class="b-search__suggest-link">Славянск</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="71" class="b-search__suggest-link">Смела</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="11" class="b-search__suggest-link">Сумы</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="20" class="b-search__suggest-link">Тернополь</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="19" class="b-search__suggest-link">Ужгород</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="53" class="b-search__suggest-link">Украинка</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="54" class="b-search__suggest-link">Умань</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="55" class="b-search__suggest-link">Фастов</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="3" class="b-search__suggest-link">Харьков</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="14" class="b-search__suggest-link">Херсон</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="15" class="b-search__suggest-link">Хмельницкий</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="64" class="b-search__suggest-link">Червоноград</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="16" class="b-search__suggest-link">Черкассы</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="8" class="b-search__suggest-link">Чернигов</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="9" class="b-search__suggest-link">Черновцы</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="66" class="b-search__suggest-link">Черноморск (Ильичевск)</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="56" class="b-search__suggest-link">Шостка</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="60" class="b-search__suggest-link">Энергодар</a>
                                    </li>

                                    <li class="b-search__dropdown-item">
                                        <a href="#" data-id="57" class="b-search__suggest-link">Яготин</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="b-search__input-holder">

                                <input
                                        class="b-search__input"
                                        placeholder="Какую работу вам нужно выполнить?"
                                        name="q"
                                        value=""
                                />
                            </div>
                            <ul class="b-search__suggest-list">
                            </ul>
                        </div>
                    </form>
                </div>
            </div>

            <div class="kb-promotion kb-promotion_type_create-task">
                <div class="kb-promotion__content">
                    <div class="kb-promotion__title">Помочь с выбором подходящего специалиста?</div>
                    <div>Привет, скорее создавайте задание и в течение 10 минут мы найдем лучшего специалиста для вашей задачи.</div>
                    <div class="kb-promotion__button-holder">
                        <a class="b-button" href="/task/create">
                            <span class="b-button__text">Оформить заявку</span>
                        </a>
                    </div>
                </div>
            </div>



            <div id="hiw" class="kb-landing-section">
                <div class="kb-landing-section__title">Как работает Kabanchik.ua</div>

                <div class="kb-how-it-works">
                    <ul class="kb-how-it-works__items">
                        <li class="kb-how-it-works__item">
                        <span class="kb-how-it-works__arrow">
                                <img src="/images/arrow-right.svg?r=9cc46986" alt="arrow-right" />
                            </span>
                            <a href="/task/create" class="kb-how-it-works__link">
                                <img class="kb-how-it-works__img" src="/images/hiw-1.png?r=25f08dae" alt="step-1" />
                                <div class="kb-how-it-works__caption">
                                    Создайте задание
                                </div>
                                Опишите, что и когда нужно сделать
                            </a>
                        </li><li class="kb-how-it-works__item">
                        <span class="kb-how-it-works__arrow">
                                <img src="/images/arrow-right.svg?r=9cc46986" alt="arrow-right" />
                            </span>
                            <a href="/task/create" class="kb-how-it-works__link">
                                <img class="kb-how-it-works__img" src="/images/hiw-2.png?r=3f1514bf" alt="step-2" />
                                <div class="kb-how-it-works__caption">
                                    Выберите специалиста
                                </div>
                                Выберите компетентного специалиста <br class="kb-hidden-tl" /> для вашей задачи
                            </a>
                        </li><li class="kb-how-it-works__item">

                            <a href="/task/create" class="kb-how-it-works__link">
                                <img class="kb-how-it-works__img" src="/images/hiw-3.png?r=01ac4853" alt="step-3" />
                                <div class="kb-how-it-works__caption">
                                    Закройте заявку
                                </div>
                                Оставьте отзыв и оценку исполнителю
                            </a>
                        </li>
                    </ul>
                    <div class="kb-how-it-works__details">
                        <a href="/task/create" target="_blank"><b>Создайте задание</b></a>
                        прямо сейчас и найдите специалиста за считанные минуты!</div>
                </div>
            </div>



            <div class="b-slider" data-bazooka="MainSlider" data-slides='[{"avatar_url": "/images/slider/reviewer1.jpg?r=354f07eb", "background_url": "/images/slider/slide_type_home-master.jpg?r=a6274d83", "job": "\u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440 \u0442\u0435\u043b\u0435\u043a\u043e\u043c\u043c\u0443\u043d\u0438\u043a\u0430\u0446\u0438\u043e\u043d\u043d\u043e\u0439 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438", "name": "\u0412\u0430\u043b\u0435\u0440\u0438\u0439", "text": "\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0439 \u0441\u0435\u0440\u0432\u0438\u0441, \u043d\u0430 \u043a\u043e\u0442\u043e\u0440\u043e\u043c \u0443\u0434\u043e\u0431\u043d\u043e \u0438 \u043a\u043b\u0438\u0435\u043d\u0442\u0430\u043c, \u0438 \u0438\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044f\u043c. \u0425\u043e\u0440\u043e\u0448\u0438\u0439 \u043f\u043e\u0434\u0431\u043e\u0440 \u0441\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442\u043e\u0432 \u0434\u043b\u044f \u0435\u0436\u0435\u0434\u043d\u0435\u0432\u043d\u044b\u0445 \u043f\u043e\u0442\u0440\u0435\u0431\u043d\u043e\u0441\u0442\u0435\u0439 \u043b\u044e\u0434\u0435\u0439 \u0434\u043e\u043f\u043e\u043b\u043d\u044f\u0435\u0442\u0441\u044f \u043f\u043e\u043d\u044f\u0442\u043d\u044b\u043c \u0438 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0442\u0438\u0432\u043d\u044b\u043c \u0438\u043d\u0442\u0435\u0440\u0444\u0435\u0439\u0441\u043e\u043c, \u0447\u0442\u043e \u0434\u0435\u043b\u0430\u0435\u0442 \"\u041a\u0430\u0431\u0430\u043d\u0447\u0438\u043a\u0430\" \u043e\u0442\u043b\u0438\u0447\u043d\u044b\u043c \u043f\u043e\u043c\u043e\u0449\u043d\u0438\u043a\u043e\u043c. \u041f\u0440\u043e\u0431\u043b\u0435\u043c\u044b \u0440\u0435\u0448\u0430\u044e\u0442\u0441\u044f \u043e\u043f\u0435\u0440\u0430\u0442\u0438\u0432\u043d\u043e \u0438, \u0433\u043b\u0430\u0432\u043d\u043e\u0435, \u0431\u0435\u0437 \u043f\u043e\u0442\u0435\u0440\u0438 \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u0438 \u043b\u0438\u0448\u043d\u0438\u0445 \u0434\u0435\u043d\u0435\u0433. \u0412\u043f\u043e\u043b\u043d\u0435 \u0434\u043e\u0432\u043e\u043b\u0435\u043d \u0441\u043e\u0442\u0440\u0443\u0434\u043d\u0438\u0447\u0435\u0441\u0442\u0432\u043e\u043c.", "url": "/user/63156"}, {"avatar_url": "https://static.kabanchik.ua/static//images/slider/reviewer2.jpg?r=e487bdc4", "background_url": "https://static.kabanchik.ua/static//images/slider/slide_type_seamstress.jpg?r=bc1f2c1e", "job": null, "name": "\u0422\u0430\u0442\u044c\u044f\u043d\u0430", "text": "\u041e\u0447\u0435\u043d\u044c \u0434\u043e\u0432\u043e\u043b\u044c\u043d\u0430 \u0440\u0435\u0441\u0443\u0440\u0441\u043e\u043c. \u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043b\u0430\u0441\u044c \u043d\u0435\u0441\u043a\u043e\u043b\u044c\u043a\u043e \u0440\u0430\u0437: \u0438\u0441\u043a\u0430\u043b\u0430 \u043c\u0430\u0441\u0442\u0435\u0440\u0430 \u0434\u043b\u044f \u043f\u043e\u043a\u043b\u0435\u0439\u043a\u0438 \u043e\u0431\u043e\u0435\u0432, \u043d\u044f\u043d\u044e \u0434\u043b\u044f \u0440\u0435\u0431\u0435\u043d\u043a\u0430 \u0438 \u0448\u0432\u0435\u044e. \u0414\u043e\u0432\u043e\u043b\u044c\u043d\u0430. \u0414\u0430\u0436\u0435 \u043d\u0435 \u0431\u044b\u043b\u043e \u0431\u043e\u044f\u0437\u043d\u043e \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u044f\u0442\u044c \u0437\u0430\u043a\u0430\u0437 \u043f\u043e\u0448\u0438\u0432\u0430 \u0432 \u0434\u0440\u0443\u0433\u043e\u0439 \u0433\u043e\u0440\u043e\u0434, \u0442\u0430\u043a \u043a\u0430\u043a \u0442\u0449\u0430\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u043f\u0440\u043e\u0432\u0435\u0440\u043a\u0430 \u0438\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u0435\u0439 \u043c\u0438\u043d\u0438\u043c\u0438\u0437\u0438\u0440\u043e\u0432\u0430\u043b\u0430 \u0440\u0438\u0441\u043a\u0438.", "url": "/user/85238"}, {"avatar_url": "https://static.kabanchik.ua/static//images/slider/reviewer3.jpg?r=9d139d9b", "background_url": "https://static.kabanchik.ua/static//images/slider/slide_type_delivery.jpg?r=ec7d007e", "job": "\u0440\u0430\u0437\u0440\u0430\u0431\u043e\u0442\u0447\u0438\u043a \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u043d\u043e\u0433\u043e \u043e\u0431\u0435\u0441\u043f\u0435\u0447\u0435\u043d\u0438\u044f", "name": "\u041e\u043b\u0435\u0441\u044f", "text": "\u041a\u0430\u0431\u0430\u043d\u0447\u0438\u043a - \u043f\u0440\u043e\u0441\u0442\u043e \u0441\u0435\u0440\u0432\u0438\u0441-\u043d\u0430\u0445\u043e\u0434\u043a\u0430! \u0412 \u0443\u0441\u043b\u043e\u0432\u0438\u044f\u0445 \u0431\u043e\u043b\u044c\u0448\u043e\u0433\u043e \u0433\u043e\u0440\u043e\u0434\u0430, \u0431\u0435\u0433\u043e\u0442\u043d\u0438 \u0438 \u0432\u0435\u0447\u043d\u043e\u0433\u043e \u0431\u0430\u043b\u0430\u043d\u0441\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f \"\u0441\u043a\u043e\u043b\u044c\u043a\u043e \u0441\u0442\u043e\u0438\u0442 \u043c\u043e\u0451 \u0432\u0440\u0435\u043c\u044f\" \u0434\u043b\u044f \u043c\u0435\u043d\u044f \u041a\u0430\u0431\u0430\u043d\u0447\u0438\u043a - \u043f\u0430\u043b\u043e\u0447\u043a\u0430-\u0432\u044b\u0440\u0443\u0447\u0430\u043b\u043e\u0447\u043a\u0430. 3 \u043c\u0438\u043d\u0443\u0442\u044b \u043d\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u0435 \u0437\u0430\u0434\u0430\u043d\u0438\u044f, 1 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u043d\u044b\u0439 \u0437\u0432\u043e\u043d\u043e\u043a \u0441 \u0438\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u0435\u043c, \u043e\u043a\u043e\u043b\u043e \u0447\u0430\u0441\u0430 \u0435\u0433\u043e \u0440\u0430\u0431\u043e\u0442\u044b \u0438 \u0432\u0441\u0435 \u0441\u0447\u0430\u0441\u0442\u043b\u0438\u0432\u044b!", "url": "/user/21789"}, {"avatar_url": "https://static.kabanchik.ua/static//images/slider/reviewer5.jpg?r=e1cc796f", "background_url": "https://static.kabanchik.ua/static//images/slider/slide_type_copyright.jpg?r=38347094", "job": "\u043a\u043e\u043f\u0438\u0440\u0430\u0439\u0442\u0435\u0440", "name": "\u0421\u0435\u0440\u0433\u0435\u0439", "text": "\u041a\u0430\u0431\u0430\u043d\u0447\u0438\u043a \u0434\u0430\u043b \u043c\u043d\u0435 \u0432\u043e\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442\u044c \u0431\u044b\u0441\u0442\u0440\u043e \u043d\u0430\u0445\u043e\u0434\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u043e\u0432 \u2013 \u044f \u043f\u0440\u043e\u0441\u0442\u043e \u043f\u043e\u043b\u0443\u0447\u0430\u044e \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f \u043e \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 \u043d\u043e\u0432\u044b\u0445 \u0437\u0430\u0434\u0430\u043d\u0438\u0439. \u041e\u043d \u043f\u043e\u0437\u0432\u043e\u043b\u0438\u043b \u0437\u0430\u0440\u0430\u0431\u0430\u0442\u044b\u0432\u0430\u0442\u044c \u0431\u043e\u043b\u044c\u0448\u0435, \u0447\u0435\u043c \u044f \u043f\u043e\u043b\u0443\u0447\u0430\u043b, \u0440\u0430\u0431\u043e\u0442\u0430\u044f \u0432 \u043e\u0444\u0438\u0441\u0435. \u041e\u043d \u043e\u0442\u043a\u0440\u044b\u043b \u043d\u043e\u0432\u044b\u0445 \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u043e\u0432 \u2013 \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u043d\u0435\u0439\u0448\u0438\u0445 \u043f\u0440\u0435\u0434\u043f\u0440\u0438\u043d\u0438\u043c\u0430\u0442\u0435\u043b\u0435\u0439. \u0412\u044b\u043f\u043e\u043b\u043d\u044f\u044f \u0438\u0445 \u0437\u0430\u0434\u0430\u0447\u0438, \u044f \u0440\u0430\u0437\u0432\u0438\u0432\u0430\u044e\u0441\u044c \u043a\u0430\u043a \u0441\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442.", "url": "/user/29947"}]'>
                <div class="b-slider__list">

                    <img class="b-slider__img" src="/images/slider/slide_type_home-master.jpg?r=a6274d83" alt="slide-bg-1">

                    <img class="b-slider__img kb-hidden" src="/images/slider/slide_type_seamstress.jpg?r=bc1f2c1e" alt="slide-bg-2">

                    <img class="b-slider__img kb-hidden" src="/images/slider/slide_type_delivery.jpg?r=ec7d007e" alt="slide-bg-3">

                    <img class="b-slider__img kb-hidden" src="/images/slider/slide_type_copyright.jpg?r=38347094" alt="slide-bg-4">

                    <div class="b-slider__comment-holder">

                        <div class="b-slider__comment">
                            <div class="b-slider__avatar">
                                <div class="b-rounded-image b-image-holder">

                                    <a href="/user/63156">
                                        <img alt="Валерий"
                                             class="b-image-holder__img"
                                             src="/images/slider/reviewer1.jpg?r=354f07eb"
                                             width="80"
                                             height="80"
                                        >
                                    </a>

                                </div>
                            </div>
                            <div class="b-slider__author-info">

                                <a class="b-slider__author" href="/user/63156">Валерий</a>


                                <div class="b-slider__job">менеджер телекоммуникационной компании</div>

                            </div>
                            <div class="b-slider__review">Популярный сервис, на котором удобно и клиентам, и исполнителям. Хороший подбор специалистов для ежедневных потребностей людей дополняется понятным и информативным интерфейсом, что делает "Кабанчика" отличным помощником. Проблемы решаются оперативно и, главное, без потери времени и лишних денег. Вполне доволен сотрудничеством.</div>
                        </div>

                        <div class="b-slider__comment kb-hidden">
                            <div class="b-slider__avatar">
                                <div class="b-rounded-image b-image-holder">

                                    <a href="/user/85238">
                                        <img alt="Татьяна"
                                             class="b-image-holder__img"
                                             src="/images/slider/reviewer2.jpg?r=e487bdc4"
                                             width="80"
                                             height="80"
                                        >
                                    </a>

                                </div>
                            </div>
                            <div class="b-slider__author-info">

                                <a class="b-slider__author" href="/user/85238">Татьяна</a>


                            </div>
                            <div class="b-slider__review">Очень довольна ресурсом. Пользовалась несколько раз: искала мастера для поклейки обоев, няню для ребенка и швею. Довольна. Даже не было боязно отправлять заказ пошива в другой город, так как тщательная проверка исполнителей минимизировала риски.</div>
                        </div>

                        <div class="b-slider__comment kb-hidden">
                            <div class="b-slider__avatar">
                                <div class="b-rounded-image b-image-holder">

                                    <a href="/user/21789">
                                        <img alt="Олеся"
                                             class="b-image-holder__img"
                                             src="/images/slider/reviewer3.jpg?r=9d139d9b"
                                             width="80"
                                             height="80"
                                        >
                                    </a>

                                </div>
                            </div>
                            <div class="b-slider__author-info">

                                <a class="b-slider__author" href="/user/21789">Олеся</a>


                                <div class="b-slider__job">разработчик программного обеспечения</div>

                            </div>
                            <div class="b-slider__review">Кабанчик - просто сервис-находка! В условиях большого города, беготни и вечного балансирования "сколько стоит моё время" для меня Кабанчик - палочка-выручалочка. 3 минуты на создание задания, 1 телефонный звонок с исполнителем, около часа его работы и все счастливы!</div>
                        </div>

                        <div class="b-slider__comment kb-hidden">
                            <div class="b-slider__avatar">
                                <div class="b-rounded-image b-image-holder">

                                    <a href="/user/29947">
                                        <img alt="Сергей"
                                             class="b-image-holder__img"
                                             src="/images/slider/reviewer5.jpg?r=e1cc796f"
                                             width="80"
                                             height="80"
                                        >
                                    </a>

                                </div>
                            </div>
                            <div class="b-slider__author-info">

                                <a class="b-slider__author" href="/user/29947">Сергей</a>


                                <div class="b-slider__job">копирайтер</div>

                            </div>
                            <div class="b-slider__review">Кабанчик дал мне возможность быстро находить заказчиков – я просто получаю сообщения о публикации новых заданий. Он позволил зарабатывать больше, чем я получал, работая в офисе. Он открыл новых заказчиков – интереснейших предпринимателей. Выполняя их задачи, я развиваюсь как специалист.</div>
                        </div>

                    </div>
                </div>
                <ul class="b-slider__paginator">

                    <li data-i="0" class="b-slider__paginator-item b-slider__paginator-item_state_active"></li>

                    <li data-i="1" class="b-slider__paginator-item"></li>

                    <li data-i="2" class="b-slider__paginator-item"></li>

                    <li data-i="3" class="b-slider__paginator-item"></li>

                </ul>
            </div>






            <div class="kb-apps-promotion">
                <div class="kb-apps-promotion__wrapper">
                    <div class="kb-apps-promotion__content">
                        <div class="kb-apps-promotion__title">
                            Kabanchik.ua — тысячи заданий в вашем смартфоне
                        </div>
                        Загружайте бесплатное приложение и заказывайте любую услугу прямо на ходу
                        <div class="kb-apps-promotion__apps-holder">
                            <ul class="kb-apps-promotion__apps">
                                <li class="kb-apps-promotion__app">
                                    <a href="https://play.google.com/store/apps/details?id=ua.kabanchik.customer&amp;referrer=utm_source%3Dsite%26utm_medium%3Dmain_page_icon" target="_blank" rel="noopener">
                                        <img
                                                src="/images/google-play-badge.png?r=00297f2a"
                                                srcset="/images/google-play-badge@2x.png?r=67cdb490"
                                                alt="Android app"
                                                width="150"
                                                height="45"
                                        />
                                    </a>
                                </li>
                                <li class="kb-apps-promotion__app">
                                    <a href="https://itunes.apple.com/app/apple-store/id1193523632?pt=59457800&amp;ct=main_page_icon&amp;mt=8" target="_blank" rel="noopener">
                                        <img
                                                src="/images/app-store-badge.png?r=a4c82819"
                                                srcset="/images/app-store-badge@2x.png?r=65f299c0"
                                                alt="iOS app"
                                                width="150"
                                                height="45"
                                        />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="kb-apps-promotion__img-holder">

                        <img class="kb-apps-promotion__img" src="/images/apps-promotion.png?r=3d32e817" alt="apps" />

                    </div>
                </div>
            </div>

            <div class="kb-landing-section">
                <div class="kb-landing-section__title">

                    География Кабанчика

                </div>
                <div class="kb-regions">
                    <ul class="kb-regions__list">

                        <li class="kb-regions__item">

                            <a href="https://aleksandriya.kabanchik.ua/"
                               class="kb-regions__link">Александрия</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://bc.kabanchik.ua/"
                               class="kb-regions__link">Белая Церковь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://bd.kabanchik.ua/"
                               class="kb-regions__link">Белгород-Днестровский</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://berdichev.kabanchik.ua/"
                               class="kb-regions__link">Бердичев</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://berdyansk.kabanchik.ua/"
                               class="kb-regions__link">Бердянск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://berezan.kabanchik.ua/"
                               class="kb-regions__link">Березань</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://borispol.kabanchik.ua/"
                               class="kb-regions__link">Борисполь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://boyarka.kabanchik.ua/"
                               class="kb-regions__link">Боярка</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://brovary.kabanchik.ua/"
                               class="kb-regions__link">Бровары</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://bucha.kabanchik.ua/"
                               class="kb-regions__link">Буча</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://vasilkov.kabanchik.ua/"
                               class="kb-regions__link">Васильков</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://vinnitsa.kabanchik.ua/"
                               class="kb-regions__link">Винница</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://vishnevoe.kabanchik.ua/"
                               class="kb-regions__link">Вишнёвое</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://vyshgorod.kabanchik.ua/"
                               class="kb-regions__link">Вышгород</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://dnepr.kabanchik.ua/"
                               class="kb-regions__link">Днепр (Днепропетровск)</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://drogobich.kabanchik.ua/"
                               class="kb-regions__link">Дрогобыч</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://zhitomir.kabanchik.ua/"
                               class="kb-regions__link">Житомир</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://zaporozhye.kabanchik.ua/"
                               class="kb-regions__link">Запорожье</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://ivano-frankovsk.kabanchik.ua/"
                               class="kb-regions__link">Ивано-Франковск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://izmail.kabanchik.ua/"
                               class="kb-regions__link">Измаил</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://irpen.kabanchik.ua/"
                               class="kb-regions__link">Ирпень</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kam-pod.kabanchik.ua/"
                               class="kb-regions__link">Каменец-Подольский</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kamenskoe.kabanchik.ua/"
                               class="kb-regions__link">Каменское (Днепродзержинск)</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kiev.kabanchik.ua/"
                               class="kb-regions__link">Киев</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kovel.kabanchik.ua/"
                               class="kb-regions__link">Ковель</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://konotop.kabanchik.ua/"
                               class="kb-regions__link">Конотоп</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kramatorsk.kabanchik.ua/"
                               class="kb-regions__link">Краматорск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kremenchug.kabanchik.ua/"
                               class="kb-regions__link">Кременчуг</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://krivoy-rog.kabanchik.ua/"
                               class="kb-regions__link">Кривой Рог</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kropivnitskiy.kabanchik.ua/"
                               class="kb-regions__link">Кропивницкий (Кировоград)</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://lisichansk.kabanchik.ua/"
                               class="kb-regions__link">Лисичанск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://losovaya.kabanchik.ua/"
                               class="kb-regions__link">Лозовая</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://lutsk.kabanchik.ua/"
                               class="kb-regions__link">Луцк</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://lvov.kabanchik.ua/"
                               class="kb-regions__link">Львов</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://mariupol.kabanchik.ua/"
                               class="kb-regions__link">Мариуполь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://melitopol.kabanchik.ua/"
                               class="kb-regions__link">Мелитополь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://mukachevo.kabanchik.ua/"
                               class="kb-regions__link">Мукачево</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://nejin.kabanchik.ua/"
                               class="kb-regions__link">Нежин</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://nikolaev.kabanchik.ua/"
                               class="kb-regions__link">Николаев</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://nikopol.kabanchik.ua/"
                               class="kb-regions__link">Никополь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://novomoskovsk.kabanchik.ua/"
                               class="kb-regions__link">Новомосковск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://obuhov.kabanchik.ua/"
                               class="kb-regions__link">Обухов</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://odessa.kabanchik.ua/"
                               class="kb-regions__link">Одесса</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://pavlograd.kabanchik.ua/"
                               class="kb-regions__link">Павлоград</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://pervomaisk.kabanchik.ua/"
                               class="kb-regions__link">Первомайск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://pereyaslav-hmelitski.kabanchik.ua/"
                               class="kb-regions__link">Переяслав-Хмельницкий</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://poltava.kabanchik.ua/"
                               class="kb-regions__link">Полтава</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://prilyki.kabanchik.ua/"
                               class="kb-regions__link">Прилуки</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://rzhishev.kabanchik.ua/"
                               class="kb-regions__link">Ржищев</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://rovno.kabanchik.ua/"
                               class="kb-regions__link">Ровно</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://severodoneck.kabanchik.ua/"
                               class="kb-regions__link">Северодонецк</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://slavutich.kabanchik.ua/"
                               class="kb-regions__link">Славутич</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://slavyansk.kabanchik.ua/"
                               class="kb-regions__link">Славянск</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://smela.kabanchik.ua/"
                               class="kb-regions__link">Смела</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://sumy.kabanchik.ua/"
                               class="kb-regions__link">Сумы</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://ternopol.kabanchik.ua/"
                               class="kb-regions__link">Тернополь</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://uzhgorod.kabanchik.ua/"
                               class="kb-regions__link">Ужгород</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://ukrainka.kabanchik.ua/"
                               class="kb-regions__link">Украинка</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://uman.kabanchik.ua/"
                               class="kb-regions__link">Умань</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://fastov.kabanchik.ua/"
                               class="kb-regions__link">Фастов</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://kharkov.kabanchik.ua/"
                               class="kb-regions__link">Харьков</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://herson.kabanchik.ua/"
                               class="kb-regions__link">Херсон</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://hmelnitskiy.kabanchik.ua/"
                               class="kb-regions__link">Хмельницкий</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://chervonograd.kabanchik.ua/"
                               class="kb-regions__link">Червоноград</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://cherkassi.kabanchik.ua/"
                               class="kb-regions__link">Черкассы</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://chernigov.kabanchik.ua/"
                               class="kb-regions__link">Чернигов</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://chernovtsi.kabanchik.ua/"
                               class="kb-regions__link">Черновцы</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://chernomorsk.kabanchik.ua/"
                               class="kb-regions__link">Черноморск (Ильичевск)</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://shostka.kabanchik.ua/"
                               class="kb-regions__link">Шостка</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://energodar.kabanchik.ua/"
                               class="kb-regions__link">Энергодар</a>

                        </li>

                        <li class="kb-regions__item">

                            <a href="https://yagotin.kabanchik.ua/"
                               class="kb-regions__link">Яготин</a>

                        </li>

                    </ul>
                </div>
            </div>

            <div class="kb-line"></div>

            <div class="kb-landing-section">
                <div class="kb-landing-section__title">
                    Ищите проверенных исполнителей или выгодные заказы
                    <br class="kb-hidden-tl">
                    с помощью сервиса Kabanchik.ua
                </div>
                <div class="kb-inline-items kb-inline-items_spacing_30">
                    <div class="kb-inline-items__item">
                        <a class="b-button" href="/task/create">
                            <span class="b-button__text">Найти исполнителя</span>
                        </a>
                    </div>
                    <div class="kb-inline-items__item">
                        <a class="b-button b-button_theme_bordered" href="https://kabanchik.ua/registration/performer">
                            <span class="b-button__text">Стать исполнителем</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kb-landing-section kb-landing-section_type_partners">
                <div class="kb-landing-section__title">Наши партнеры</div>
                <div class="kb-partners">
                    <ul class="kb-partners__list">
                        <li class="kb-partners__item">
                            <a class="kb-partners__link kb-partners__link_type_lm"
                               href="https://www.leroymerlin.ua/?utm_source=kabanchik.ua&utm_medium=referral&utm_campaign=partners_block"
                               target="_blank"
                               title="Кабанчик рекомендує купувати товари для будівництва, ремонту, декору та саду у Леруа Мерлен"
                            ></a>
                        </li>
                        <li class="kb-partners__item">
                            <a class="kb-partners__link kb-partners__link_type_prom" href="https://prom.ua/" target="_blank"></a>
                        </li>
                        <li class="kb-partners__item">
                            <a class="kb-partners__link kb-partners__link_type_bigl" href="https://bigl.ua/" target="_blank"></a>
                        </li>
                        <li class="kb-partners__item">
                            <a class="kb-partners__link kb-partners__link_type_pb" href="https://privatbank.ua/ru/news/privat24-dajet-rabotu-santehnikam/" target="_blank"></a>
                        </li>
                    </ul>
                </div>
            </div>

            <script type="text/javascript">
                window._retag = window._retag || [];
                window._retag.push({code: "9ce888464e", level: 0});
                (function () {
                    var id = "admitad-retag";
                    if (document.getElementById(id)) {return;}
                    var s = document.createElement("script");
                    s.async = true; s.id = id;
                    var r = (new Date).getDate();
                    s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.lenmit.com/static//js/retag.js?r="+r;
                    var a = document.getElementsByTagName("script")[0]
                    a.parentNode.insertBefore(s, a);
                })()
            </script>




        </div>
    </div>
    <div class="kb-layout__row">

        <div class="kb-page">

            <div class="kb-footer qa-footer">
                <div class="kb-footer__column kb-footer__column_number_1">
                    <a href="https://kabanchik.ua/" target="_blank">
                        <img
                                class="kb-footer__logo"
                                src="/images/logo/logo_white.png?r=6cc42d7a"
                                srcset="https://static.kabanchik.ua/static//images/logo/logo2x_white.png?r=83da027d 2x"
                                width="143"
                                height="34">
                    </a>
                    <div class="kb-footer__socials">
                        <ul class="b-socials b-socials_type_rounded b-socials_theme_white">
                            <li class="b-socials__item-holder">
                                <a class="b-socials__item b-socials__item_theme_facebook" href="https://www.facebook.com/kabanchik.ua" target="_blank"></a>
                            </li>

                            <li class="b-socials__item-holder">
                                <a class="b-socials__item b-socials__item_theme_gplus" href="https://plus.google.com/+KabanchikUaPlus" target="_blank"></a>
                            </li>

                            <li class="b-socials__item-holder">
                                <a class="b-socials__item b-socials__item_theme_instagram" href="https://www.instagram.com/kabanchik.ua" target="_blank"></a>
                            </li>

                            <li class="b-socials__item-holder">
                                <a class="b-socials__item b-socials__item_theme_youtube" href="https://www.youtube.com/c/KabanchikUaPlus" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="kb-footer-contacts">
                        <div class="kb-footer-contacts__section">
                            <div class="kb-footer-contacts__text-holder">
                                <div class="kb-footer-contacts__title">Контакты службы поддержки</div>
                            </div>
                        </div>
                        <div class="kb-footer-contacts__section">
                            <div class="kb-footer-contacts__icon glyph-email"></div>
                            <div class="kb-footer-contacts__text-holder">
                                <a class="kb-footer__link kb-footer__link_color_white" href="mailto:support@kabanchik.ua">support@kabanchik.ua</a>
                            </div>
                        </div>
                        <div class="kb-footer-contacts__section">
                            <div class="kb-footer-contacts__icon glyph-clock"></div>
                            <div class="kb-footer-contacts__text-holder">
                                Пн — Пт: c 8:00 до 20:00
                                <br>
                                Сб — Вс: c 9:00 до 18:00
                            </div>
                        </div>

                    </div>
                </div>

                <dl class="kb-footer__column kb-footer__column_number_2">

                    <dt class="kb-footer__item">О нас</dt>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/about">О проекте</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/contacts">Контакты</a>
                    </dd>

                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/apps">Мобильное приложение</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="http://kabanchik.ua.tilda.ws">Дневник Кабанчика</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="http://kabanchik.by">Мы в Беларуси</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="http://megamaster.kz">Мы в Казахстане</a>
                    </dd>


                </dl>

                <dl class="kb-footer__column kb-footer__column_number_3">
                    <dt class="kb-footer__item">Как это работает</dt>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/how-it-works">Как заказать услугу</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/rabota">Как стать исполнителем</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/for-business">Преимущества для компаний</a>
                    </dd>

                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/for-entrepreneurs">Как зарегистрировать ФЛП</a>
                    </dd>


                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/safety">Гарантия и безопасность</a>
                    </dd>

                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/interesting-tasks">Интересные задания</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/last-reviews">Последние отзывы</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/top-performers">Топ исполнителей</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="https://kiev.kabanchik.ua/all-categories">Все категории услуг</a>
                    </dd>
                </dl>

                <dl class="kb-footer__column kb-footer__column_number_4">
                    <dt class="kb-footer__item">Помощь</dt>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/help">Вопросы и ответы</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/terms">Публичная оферта</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/privacy-policy">Правила конфиденциальности</a>
                    </dd>
                    <dd class="kb-footer__item">
                        <a class="kb-footer__link" href="/contacts">Служба поддержки</a>
                    </dd>
                </dl>
            </div>

            <div class="kb-footer kb-footer_size_small">
                <div class="kb-footer__left-section">
                    <div class="kb-footer__copyright-text">
                        Kabanchik © 2012-2018. All rights reserved.
                    </div>
                    <div class="kb-footer__copyright-text">Сайт на 100% сделан из повторно переработанных пикселей.</div>
                </div>




                <div class="kb-footer__right-section">
                    <div class="kb-footer__apps">
                        <div class="kb-footer__apps-title">Скачайте наше приложение для заказа услуг</div>
                        <ul class="kb-footer__apps-list">

                            <li class="kb-footer__app">
                                <a href="https://play.google.com/store/apps/details?id=ua.kabanchik.customer&amp;referrer=utm_source%3Dsite%26utm_medium%3Dfooter_icon">
                                    <img
                                            src="/images/google-play-badge.png?r=00297f2a"
                                            srcset="/images/google-play-badge@2x.png?r=67cdb490"
                                            alt="Android app"
                                            width="150"
                                            height="45"
                                    >
                                </a>
                            </li>


                            <li class="kb-footer__app">
                                <a href="https://itunes.apple.com/app/apple-store/id1193523632?pt=59457800&amp;ct=footer_icon&amp;mt=8">

                                    <img
                                            src="/images/app-store-badge.png?r=a4c82819"
                                            srcset="/images/app-store-badge@2x.png?r=65f299c0 2x"
                                            alt="iOS app"
                                            width="150"
                                            height="45"
                                    >
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div data-bazooka="Notifications"></div>




<script>window.__CONFIG = {"ENV": {"server_name": "kabanchik.ua", "support_mail": "support@kabanchik.ua", "domain_name": "Kabanchik.ua"}, "USER": {"id": null, "is_authenticated": false}, "COUNTRY": {"phone_mask": "+38(999)999-9999", "currency": "\u0433\u0440\u043d"}, "URL": {"upload": "/file/upload", "avatar_url": "https://static.kabanchik.ua/static//images/no-avatar.png?r=442985ba", "category_url": "https://static.kabanchik.ua/static//images/no-category.jpg?r=89357c88", "company_url": "https://static.kabanchik.ua/static//images/logo-default-company.png?r=444f9658"}, "FILE": {"max_size": 10485760}, "CASHBACK": {"max_cash_back_amount": 250, "cash_back_ratio": "0.05", "round_up": true}}</script>

<script crossorigin="anonymous" defer src="/js/common.js?r=a2ef031a"></script>
<script crossorigin="anonymous" defer src="/js/core.js?r=994e73e8"></script>




<script>
    (function() {
        var widget_id = "k8AFlvL8Ad";var d=document;var w=window;
        function l() {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/'+widget_id;
            var ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);
        }
        if(d.readyState=='complete'){
            l();
        } else {
            if(w.attachEvent) {
                w.attachEvent('onload',l);
            } else {
                w.addEventListener('load',l,false);
            }
        }
    })();
    function jivo_onLoadCallback() {

    };
</script>

<script type="text/javascript">
    window.GOTCHA_HOST = "https://tracker.prom.ua/errors_/js/log.js";
    window.GOTCHA_SITE = "Kabanchik.ua";
    window.GOTCHA_TXID = "522150f2-30f6-4c24-8f61-20dfdc2d8b96";
</script>
<script async type="text/javascript" src="https://cdn.prom.st/gotcha/gotcha-min.js"></script>

</body>
</html>
<!-- version: v11.7.2-6-gbbe8e64 -->