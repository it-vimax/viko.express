<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');



Auth::routes();

Route::get('/specialist', 'Specialist\HomeController@index')->name('specialist');

Route::get('/auth/net', 'TestAuthController@index')->name('testAuth');
Route::get('auth/net/{provider}', 'TestAuthController@redirectToProvider'); // регистрация через google
Route::get('auth/net/{provider}/callback', 'TestAuthController@redirectToProviderCallback'); // ответ от net